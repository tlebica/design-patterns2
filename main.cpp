#include <iostream>
#include "Factory.hpp"


int main()
{

    auto exp = create();
    std::cout << "Result is " << exp(1,2,3,4,5) << std::endl;
    std::cout << "Result is " << exp() << std::endl;
    std::cout << "Result is " << exp(3,4,7,9,2) << std::endl;

    return 0;
}
