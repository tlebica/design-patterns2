#pragma once
#include "Nodes.hpp"
#include <initializer_list>
#include <vector>
#include <algorithm>

class Expression
{
public:
    Expression(NodePtr mainNode, std::initializer_list<ValuePtr> values)
        :values(values.begin(), values.end()),
         mainNode(mainNode)
    {}

    std::vector<int> createVector()
    {
        return std::vector<int>{};
    }

    template<typename ... T>
    std::vector<int> createVector(int val, T ... values)
    {
        std::vector<int> result = createVector(values...);
        result.push_back(val);
        return result;
    }

    template<typename ... T>
    int operator()(T... newValues)
    {
        std::vector<int> vectorValues = createVector(newValues...);
        std::reverse(vectorValues.begin(), vectorValues.end());
        int i = 0;

        for (auto& newValue: vectorValues)
        {
            values[i++]->setValue(newValue);
            if (i >= values.size())
                break;
        }

        return mainNode->getResult();
    }

private:
    std::vector<ValuePtr> values;
    NodePtr mainNode;
};
