#include "Factory.hpp"

Expression create()
{
    ValuePtr a = std::make_shared<Value>();
    ValuePtr b = std::make_shared<Value>();
    ValuePtr c = std::make_shared<Value>();
    ValuePtr d = std::make_shared<Value>();
    ValuePtr e = std::make_shared<Value>();

    NodePtr aa = std::make_shared<ResultOutputNode>(a);
    NodePtr bb = std::make_shared<ResultOutputNode>(b);
    NodePtr ee = std::make_shared<ResultOutputNode>(e);

    NodePtr mul = std::make_shared<ResultOutputNode>(std::make_shared<Action>(c, d, std::multiplies<int>{}));
    NodePtr div = std::make_shared<Action>(mul, ee, std::divides<int>{});
    NodePtr addition = std::make_shared<Action>(aa, bb, std::plus<int>{});
    NodePtr sub = std::make_shared<Action>(addition, div, std::minus<int>{});

    return Expression(sub,{a,b,c,d,e});
}
