#pragma once
#include <iostream>
#include <memory>
#include <functional>

struct Node
{
    virtual int getResult() = 0;
};

using NodePtr = std::shared_ptr<Node>;

using Operation = std::function<int(int, int)>;

class Action : public Node
{
public:
    Action(NodePtr childA, NodePtr childB, Operation operation)
        :childA(childA),
         childB(childB),
         operation(operation)
    {}
    int getResult() override { return operation(childA->getResult(), childB->getResult()); }
private:
    NodePtr childA;
    NodePtr childB;
    Operation operation;
};

class Value : public Node
{
public:
    int getResult() override { return value; }
    void setValue(int value)
    {
        this->value = value;
    }
private:
    int value = 0;
};

using ValuePtr = std::shared_ptr<Value>;

class ResultOutputNode : public Node
{
public:
    ResultOutputNode(NodePtr node)
        :node(node)
    {}

    int getResult() override
    {
        int result = node->getResult();
        std::cout << "Partial result is: " << result << std::endl;
        return result;
    }

private:
    NodePtr node;
};
